# Emotional Mario by Team14
## Requirements
- Cuda 11.3 with CuDNN
- Python 3.7
- PyTorch on 1.11.0
- torchvision on 0.12.0
- pandas
- PIL
- tqdm
- numpy
  

## Demo
### create conda env
For ubuntu
```
conda env create -f example.yml
```
or  
For windows
```
conda env create -f example2.yml
```

### Run model
```
python model.py -i [input files directory path] -o [output file]
path]
```
e.g.
```
python model.py -i ./participants_0 -o participant_0_events.csv
```