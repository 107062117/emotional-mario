import csv


# file is a string of your filename.csv
def delete_duplicate(file):
    with open(file, newline='') as csvfile:
        rows = csv.reader(csvfile)
        
        output = []
        # start is the first index, end is the last
        start = 0
        end = 0
        
        # cur is current event
        cur_event = ''
        
        init = 1
        pre_flag_reached = 0
        for row in rows:
            if init == 1:
                init = 0
                start = int(row[0])
                end = int(row[0])
                cur_event = row[1]
                continue
            
            if int(row[0]) <= end + 25 and row[1] == cur_event:
                end = int(row[0])
            else:
                middle = str(start)
                if cur_event == "flag_reached":
                    if start > pre_flag_reached + 60 * 25:
                        output.append([middle, cur_event])
                        output.append([str(start+1), "new_stage"])
                        pre_flag_reached = start
                elif cur_event != "new_stage":
                    output.append([middle, cur_event])
                cur_event = row[1]
                start = int(row[0])
                end = int(row[0])
        
        
        print(output)
        with open(file ,'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for out in output:
                writer.writerow(out)


