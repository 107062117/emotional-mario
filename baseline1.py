
import matplotlib.pyplot as plt
from tqdm import tqdm
import random
import gc
import os
import glob
import numpy as np
import json
import csv
from pandas import read_csv
#from scipy.signal import argrelextrema
import cv2
#from data_loadertmp import GameImageLoaderTmp
from data_loader import GameImageLoader, EvalImageLoader
import os.path as ops
from torchvision.transforms import ToTensor, Compose, Pad, RandomHorizontalFlip, CenterCrop, RandomCrop, ToPILImage , Resize, RandomAffine
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
from torchvision import models
from models import resnet50_encoder

classes = ['new_stage', 'flag_reached', 'status_up','status_down','life_lost', 'no_event']
totalTrainLossList=[]
totalTrainAccList=[]
totalTestLossList=[]
totalTestAccList=[]
#define transformation
crop = 200
rng = np.random.RandomState(123)
precrop = crop + 24
crop = rng.randint(crop, precrop)
transformation = Compose([
                Resize((256,256)),
                Pad((24,24,24,24)),
                CenterCrop(precrop),
                RandomCrop(crop),
                Resize((256,256)), 
                RandomHorizontalFlip(),
                RandomAffine(degrees = 0, translate = (0.5, 0.5)),
                ToTensor()
                ])

transformation_eval = Compose([
                Resize((256,256)),
                Pad((24,24,24,24)),
                CenterCrop(precrop),
                Resize((256,256)),
                ToTensor()
                ])

#load dataset
game_image_sets = GameImageLoader(transformation)
eval_image_sets = EvalImageLoader(transformation_eval)

train_size = int(0.8 * len(game_image_sets))
test_size = len(game_image_sets) - train_size
train_dataset, test_dataset = torch.utils.data.random_split(game_image_sets, [train_size, test_size])

train_dataset_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                                batch_size=16,
                                                shuffle=True, num_workers =8)

test_dataset_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                                batch_size=16,
                                                shuffle=True, num_workers = 8)

eval_dataset_loader = torch.utils.data.DataLoader(dataset=eval_image_sets, batch_size = 1, shuffle = False, num_workers = 8)
    
model = resnet50_encoder()
model.cuda()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

weight = []
with open("../toadstool/stastic.txt", "r") as f:
    lines = f.readlines()
    for line in lines:
        data = line.split(':')
        weight.append(int(data[-1]))

weight = [1 - (w / sum(weight)) for w in weight]
weights = torch.FloatTensor(weight).to("cuda")
criterion = nn.CrossEntropyLoss(weight = weights)
loss_value = []

def checkpoint(model, my_save_path, epoch, best_typ):
    print ("save model, current epoch:{0}".format(epoch))    
    save_epoch = epoch + 0
    if(best_typ == 'loss'):
        final_save_path = my_save_path + 'best_loss' + '.pth'
    elif(best_typ == 'acc'):
        final_save_path = my_save_path + 'best_acc' + '.pth'
    elif(best_typ == 'cur'):
        final_save_path = my_save_path + '_' + str(save_epoch) + '.pth'
    checkpoint_state = {
            'state_dict' : model.state_dict(), 
            'optimizer' : optimizer.state_dict()
            }

    torch.save(checkpoint_state, final_save_path)

#train model
def train_model(model, epoch):
    total_loss=0.0
    total=0
    accuracy=0.0
    
    for index, train_data in enumerate(tqdm(train_dataset_loader)):
        
        optimizer.zero_grad()
        input_images = Variable(torch.FloatTensor(train_data['img'])).to("cuda")
        labels = Variable(torch.LongTensor(train_data['gt'])).to("cuda")
        bvp = Variable(torch.FloatTensor(train_data['bvp'])).to("cuda")
        eda = Variable(torch.FloatTensor(train_data['eda'])).to("cuda")
        hr = Variable(torch.FloatTensor(train_data['hr'])).to("cuda")
        output = model(input_images, bvp, eda, hr)

        _, predicted = torch.max(output, 1)

        total += 16
        accuracy += (predicted == labels).sum().item()
        loss = criterion(output, labels)
        loss.backward()
        optimizer.step()
        total_loss += loss.item()
        loss_value.append(loss.item())

    total_loss = total_loss * 1.0 / len(train_dataset_loader)
    print("train loss at the epoch %d is %f"%(epoch, total_loss))
    accuracy = (100 * accuracy / total)
    print("train acc at the epoch %d is %f"%(epoch,accuracy))

    totalTrainAccList.append(accuracy)
    totalTrainLossList.append(total_loss)

    return total_loss, accuracy

#test model

def test_model(model, epoch):
    total_loss=0.0
    total=0
    accuracy=0.0
    with torch.no_grad():
        for i, test_data in enumerate(tqdm(test_dataset_loader)):
            input_images = Variable(torch.FloatTensor(test_data['img'])).to("cuda")
            labels = Variable(torch.LongTensor(test_data['gt'])).to("cuda")
            bvp = Variable(torch.FloatTensor(test_data['bvp'])).to("cuda")
            eda = Variable(torch.FloatTensor(test_data['eda'])).to("cuda")
            hr = Variable(torch.FloatTensor(test_data['hr'])).to("cuda")
            output = model(input_images, bvp, eda, hr)
            _, predicted = torch.max(output, 1)
            total += 16
            accuracy += (predicted == labels).sum().item()
            loss = criterion(output, labels)
            total_loss += loss.item()

        total_loss = total_loss * 1.0 / len(test_dataset_loader)
        print("test loss at the epoch %d is %f"%(epoch, total_loss))
        accuracy = (100 * accuracy / total)
        print("test acc at the epoch %d is %f"%(epoch,accuracy))
        totalTestAccList.append(accuracy)
        totalTestLossList.append(total_loss)

        return total_loss, accuracy

def eval_model(model):
    total_loss=0.0
    total=0
    accuracy=0.0

    with torch.no_grad():
        for i, eval_data in enumerate(tqdm(eval_dataset_loader)):
            input_images = Variable(torch.FloatTensor(eval_data['img'])).to("cuda")
            labels = Variable(torch.LongTensor(eval_data['gt'])).to("cuda")
            bvp = Variable(torch.FloatTensor(eval_data['bvp'])).to("cuda")
            eda = Variable(torch.FloatTensor(eval_data['eda'])).to("cuda")
            hr = Variable(torch.FloatTensor(eval_data['hr'])).to("cuda")
            output = model(input_images, bvp, eda, hr)
            _, predicted = torch.max(output, 1)
            print(int(predicted), int(labels))
            total += 1
            accuracy += (predicted == labels).sum().item()
            loss = criterion(output, labels)
            total_loss += loss.item()

        total_loss = total_loss * 1.0 / len(eval_dataset_loader)
        print("eval loss is %f"%(total_loss))
        accuracy = (100 * accuracy / total)
        print("eval acc is %f"%(accuracy))
        

if __name__ == "__main__":
    
    '''
    ### Trainiang ###
    best_loss = 1e22
    best_acc = -1e22
    #ch = torch.load("./save_model/_699.pth")
    #model.load_state_dict(ch['state_dict'])
    #optimizer.load_state_dict(ch['optimizer'])
    for epoch in range(800):
        model.train()
        train_loss, train_accuracy = train_model(model, epoch)
        model.eval()
        test_loss, test_accuracy=test_model(model, epoch)
        if train_accuracy>best_acc:
            checkpoint(model, './save_model/', epoch, 'acc')
        if train_loss<best_loss:
            checkpoint(model, './save_model/', epoch, 'loss')
        if epoch % 20 == 0 or epoch == 799:
            checkpoint(model, './save_model/', epoch, 'cur')
    plt.plot(loss_value)
    plt.savefig('./loss.png')
    '''
    ### Evaluating ###
    checkpoint = torch.load("./save_model/best_acc.pth")
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    
    model.eval()
    eval_model(model)
    