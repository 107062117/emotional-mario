import cv2
import numpy as np
import os
import glob
import csv
from pandas import read_csv
import json
import shutil
import random


def read_file(file_path, type):
    if type == "csv":
        data = read_csv(file_path)
        return data.values
    elif type == "json":
        f = open(file_path)
        data = json.load(f)
        return data


if __name__ == "__main__":
    participant_idx_list = ['0', '1', '3', '5', '6', '8', '9']
    # event we have
    event = {0:"new_stage", 1:"flag_reached", 2:"status_up", 3:"status_down", 4:"life_lost", 5:"no_event"}
    for participant_idx in participant_idx_list:
        # parser
        participants_path = 'D:/toadstool/participants'
        groundtruth_path = 'D:/toadstool/new_truth'
        output_path = 'D:/toadstool/new_train'

        # file path
        session_path = f'{participants_path}/participant_{participant_idx}/participant_{participant_idx}_session.json'
        bvp_path = f'{participants_path}/participant_{participant_idx}/participant_{participant_idx}_sensor/BVP.csv'
        eda_path = f'{participants_path}/participant_{participant_idx}/participant_{participant_idx}_sensor/EDA.csv'
        hr_path = f'{participants_path}/participant_{participant_idx}/participant_{participant_idx}_sensor/HR.csv'
        gt_path = f'{groundtruth_path}/participant_{participant_idx}_events.json'

        # game img path
        game_img_path = f'{participants_path}/participant_{participant_idx}/game'
        face_img_path = f'{participants_path}/participant_{participant_idx}/face'

        # game frame number
        game = read_file(session_path, "json")
        game_len = int(game["obs_n"])
        print(f'game: {game_len}')

        # read data
        bvp = read_file(bvp_path, "csv")
        eda = read_file(eda_path, "csv")
        hr = read_file(hr_path, "csv")
        gt = read_file(gt_path, "json")
        gt_index = []
        new_stage_index = []
        flag_reach_index = []
        status_up_index = []
        status_down_index = []
        life_lost_index = []

        for data in gt:
            if data['event']=="flag_reached":
                flag_reach_index.append(data['frame_number'] // 2 - 1)
            elif data['event']=="new_stage":
                new_stage_index.append(data['frame_number'] // 2)
            elif data['event']=="status_up":
                status_up_index.append(data['frame_number'] // 2)
            elif data['event']=="status_down":
                status_down_index.append(data['frame_number'] // 2)
            elif data['event']=="life_lost":
                life_lost_index.append(data['frame_number'] // 2)
            

        gt_index = flag_reach_index + new_stage_index + status_up_index + status_down_index + life_lost_index
        print(f'gt: {len(gt_index)}')
        gt_index = list(set(gt_index))
        print(f'gt: {len(gt_index)}')
        
        # no event data
        all_index = list(np.arange(0, game_len // 2, step = 1, dtype="int"))
        no_evnet_index = [idx for idx in all_index if idx not in gt_index]
        print(f'no_evnet: {len(no_evnet_index)}')

        # choose no-event data
        random_choose_event = random.sample(no_evnet_index, len(gt_index)*3)
        print(f'sample: {len(random_choose_event)}')
        random_choose_event = list(set(random_choose_event))
        all_event = sorted(random_choose_event + gt_index)

        if not os.path.exists(os.path.join(output_path, "participant_"+participant_idx, "game")):
            os.mkdir(os.path.join(output_path, "participant_"+participant_idx, "game"))
        #if not os.path.exists(os.path.join(output_path, "participant_"+participant_idx, "face")):
            #os.mkdir(os.path.join(output_path, "participant_"+participant_idx, "face"))
        # copy choosen event image
        for idx in all_event:
            shutil.copyfile(os.path.join(participants_path, "participant_"+participant_idx, "game", f"game_{idx}.png"), os.path.join(output_path, "participant_"+participant_idx, "game", f"game_{idx}.png"))
            #shutil.copyfile(os.path.join(participants_path, "participant_"+participant_idx, "face", f"face_{idx}.png"), os.path.join(output_path, "participant_"+participant_idx, "face", f"face_{idx}.png"))

        # choosen bvp event
        sensor_data_path = os.path.join(output_path, "participant_"+participant_idx, "participant_"+participant_idx+"_sensor")
        if not os.path.exists(sensor_data_path):
            os.mkdir(sensor_data_path)
        
        with open(os.path.join(sensor_data_path, 'BVP.csv'), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for idx in all_event:
                writer.writerow([bvp[idx * 2][0]])

        with open(os.path.join(sensor_data_path, 'EDA.csv'), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for idx in all_event:
                writer.writerow([eda[idx * 2][0]])

        with open(os.path.join(sensor_data_path, 'HR.csv'), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for idx in all_event:
                writer.writerow([hr[idx * 2][0]])
        
        with open(os.path.join(sensor_data_path, 'ALL.csv'), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for idx in all_event:
                e = None
                if idx in flag_reach_index:
                    e = "flag_reached"
                elif idx in new_stage_index:
                    e = "new_stage"
                elif idx in status_up_index:
                    e = "status_up"
                elif idx in status_down_index:
                    e = "status_down"
                elif idx in life_lost_index:
                    e = "life_lost"
                
                if e is not None:
                    writer.writerow([idx , bvp[idx * 2][0], eda[idx * 2][0], hr[idx * 2][0], e])
                else:
                    writer.writerow([idx , bvp[idx * 2][0], eda[idx * 2][0], hr[idx * 2][0], "no_event"])
            
                    