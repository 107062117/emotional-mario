import os
import csv
from pandas import read_csv
import random
import shutil

if __name__ == "__main__":
    participant_idx_list = ['0', '1', '3', '5', '6', '8', '9']
    event = {0:"new_stage", 1:"flag_reached", 2:"status_up", 3:"status_down", 4:"life_lost", 5:"no_event"}
    ns_total  = fr_total = ss_total = sd_total = ls_total = ne_total = 0
    for participant_idx in participant_idx_list:
        ns = []
        fr = []
        ss = []
        sd = []
        ls = []
        ne = []

        train_data_path = 'D:/toadstool/new_train'
        output_path = 'D:/toadstool'

        data_path = f'{train_data_path}/participant_{participant_idx}/participant_{participant_idx}_sensor/ALL.csv'

        data = read_csv(os.path.join(data_path), header=None)
        data.columns=['frame', 'bvp', 'eda', 'hr', 'stage']
        index = 0
        for stage in data["stage"]:
            if stage == "new_stage":
                ns.append(index)
            if stage == "flag_reached":
                fr.append(index)
            if stage == "status_up":
                ss.append(index)
            if stage == "status_down":
                sd.append(index)
            if stage == "life_lost":
                ls.append(index)
            if stage == "no_event":
                ne.append(index)
            index += 1
            
        ns_total += len(ns) 
        fr_total += len(fr) 
        ss_total += len(ss)
        sd_total += len(sd) 
        ls_total += len(ls) 
        ne_total += len(ne)
        

    with open(output_path + '/stastic.txt', 'w') as f:
        f.writelines(f'new_stage: {ns_total}\n')
        f.writelines(f"flag_reached: {fr_total}\n")
        f.writelines(f"status_up: {ss_total}\n")
        f.writelines(f"stauts_down: {sd_total}\n")
        f.writelines(f"life_lost: {ls_total}\n")
        f.writelines(f'no_event: {ne_total}\n')