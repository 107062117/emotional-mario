import pandas as pd
import numpy as np
from PIL import Image
import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision
from torchvision import transforms
from torch.utils.data import  Dataset  # For custom datasets
from torchvision.transforms import ToTensor, Compose, Pad, RandomHorizontalFlip, CenterCrop, RandomCrop, ToPILImage , Resize
from torchvision.transforms import ToPILImage
import os
from pandas import read_csv

classes = {'new_stage':0, 'flag_reached':1, 'status_up':2,'status_down':3,'life_lost':4}


class testDataLoader(Dataset):
    def __init__(self, transformation, input_path):
        
        self.imgs=[]
        self.frame=[]
        participant=os.path.split(input_path)[1]
        files = os.listdir(input_path+"/"+participant+"_game_frame")

        files.sort(key=lambda x: int(x[5:].split('.')[0]))
        for f in files:
            self.imgs.append(os.path.join(input_path+"/"+participant+"_game_frame", f))
            self.frame.append(int(f[5:-4]))
        
        bvp = read_csv(os.path.join(input_path, participant+"_sensor", "BVP.csv"), header=None)
        bvp.columns=['value']
        self.bvp = bvp['value'].tolist()
        self.bvp = np.asarray(self.bvp, dtype = 'float32')

        eda=read_csv(os.path.join(input_path, participant+"_sensor", "EDA.csv"), header=None)
        eda.columns=['value']
        self.eda = eda['value'].tolist()
        self.eda = np.asarray(self.eda, dtype = 'float32')

        hr=read_csv(os.path.join(input_path, participant+"_sensor", "HR.csv"), header=None)
        hr.columns=['value']
        self.hr = hr['value'].tolist()
        self.hr = np.asarray(self.hr, dtype = 'float32')

        self.data_len = len(files)
        self.transform = transformation
        
    def __getitem__(self, index):
        
        img_path=self.imgs[index]
        try:
            img_f= Image.open(img_path)
            

        except FileNotFoundError:
            print("sample missing use first")
            return self.__getitem__(0)
        
        img = self.transform(img_f)
        img_f.close()
        bvp = self.bvp[index]
        eda = self.bvp[index]
        hr = self.bvp[index]
        frame = index
        return {
            "img":img, 
            "bvp":bvp, 
            "frame_number":frame,
            'eda':eda,
            'hr':hr
        }
    
    def __len__(self):
        return int(self.data_len)
