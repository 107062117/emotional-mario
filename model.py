from tqdm import tqdm
import numpy as np
import csv
from pandas import read_csv
from EvalDataLoader import testDataLoader
from torchvision.transforms import ToTensor, Compose, Pad, RandomHorizontalFlip, CenterCrop, RandomCrop, ToPILImage , Resize, RandomAffine
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
from torchvision import models
from models import resnet50_encoder
import argparse
from merge_second import delete_duplicate

classes = ['new_stage', 'flag_reached', 'status_up','status_down','life_lost', 'no_event']

#define transformation
crop = 200
rng = np.random.RandomState(123)
precrop = crop + 24
crop = rng.randint(crop, precrop)
transformation = Compose([
                Resize((256,256)),
                Pad((24,24,24,24)),
                CenterCrop(precrop),
                RandomCrop(crop),
                Resize((256,256)), 
                RandomHorizontalFlip(),
                RandomAffine(degrees = 0, translate = (0.5, 0.5)),
                ToTensor()
                ])

transformation_eval = Compose([
                Resize((256,256)),
                Pad((24,24,24,24)),
                CenterCrop(precrop),
                Resize((256,256)),
                ToTensor()
                ])

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("-i", "--input-path", type=str, default=None)
argument_parser.add_argument("-o", "--output-path", type=str, default=None)
args = argument_parser.parse_args()

#load dataset

eval_image_sets = testDataLoader(transformation_eval, args.input_path)

eval_dataset_loader = torch.utils.data.DataLoader(dataset=eval_image_sets, batch_size = 64, shuffle = False, num_workers = 8)
    
model = resnet50_encoder()
model.cuda()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

def eval_model(model):
    result = open(args.output_path, 'w', newline='')
    
    writer = csv.writer(result)
    
    with torch.no_grad():
        for i, eval_data in enumerate(tqdm(eval_dataset_loader)):
            input_images = Variable(torch.FloatTensor(eval_data['img'])).to("cuda")
            bvp = Variable(torch.FloatTensor(eval_data['bvp'])).to("cuda")
            eda = Variable(torch.FloatTensor(eval_data['eda'])).to("cuda")
            hr = Variable(torch.FloatTensor(eval_data['hr'])).to("cuda")
            frame = eval_data['frame_number']
            output = model(input_images, bvp, eda, hr)
            _, predicted = torch.max(output, 1)
            p_list = predicted.tolist()
            for i in range(len(p_list)):
                if int(p_list[i]) != 5:
                    writer.writerow([int(frame[i]), classes[int(p_list[i])]])
        
if __name__ == "__main__":

    checkpoint = torch.load("./save_model/_600.pth")
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    model.eval()
    eval_model(model)
    delete_duplicate(args.output_path)
