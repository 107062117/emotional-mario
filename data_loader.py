from importlib.resources import path
import pandas as pd
import numpy as np
from PIL import Image
import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision
from torchvision import transforms
from torch.utils.data import  Dataset  # For custom datasets
from torchvision.transforms import ToTensor, Compose, Pad, RandomHorizontalFlip, CenterCrop, RandomCrop, ToPILImage , Resize
from torchvision.transforms import ToPILImage
import matplotlib.pyplot as plt
import os, sys
import random
from pandas import read_csv
import json

participants_path = '../toadstool/new_train'
eval_path = '../toadstool/new_eval'
ground_truth_path='./ground_truth'
classes = {'new_stage':0, 'flag_reached':1, 'status_up':2,'status_down':3,'life_lost':4, 'no_event':5}

class GameImageLoader(Dataset):
    def __init__(self, transformation):
        self.transform = transformation
        self.imgs=[]
        self.gt = []
        self.bvp = []
        self.eda = []
        self.hr = []

        pars = os.listdir(participants_path)
        for par in pars:
            game_dir = os.path.join(participants_path, par, "game")
            sensor_dir = os.path.join(participants_path, par, par + "_sensor")

            # game_frame
            file = os.listdir(game_dir)
            file.sort(key=lambda x: int(x[5:].split('.')[0]))

            ground_truth = read_csv(os.path.join(sensor_dir, "ALL.csv"), header=None)
            ground_truth.columns=['frame', 'bvp', 'eda', 'hr', 'stage']

            bvp = read_csv(os.path.join(sensor_dir, "BVP.csv"), header=None)
            bvp.columns=['bvp']

            eda = read_csv(os.path.join(sensor_dir, "EDA.csv"), header=None)
            eda.columns=['eda']

            hr = read_csv(os.path.join(sensor_dir, "HR.csv"), header=None)
            hr.columns=['hr']

            i = 0
            for i in range(len(file)):
                self.imgs.append(os.path.join(game_dir, file[i]))
                self.gt.append(classes[ground_truth["stage"][i]])
                self.bvp.append(bvp['bvp'][i])
                self.eda.append(eda['eda'][i])
                self.hr.append(hr['hr'][i])
                i+=1
        self.eda = np.asarray(self.eda, dtype = 'float32')
        self.bvp = np.asarray(self.bvp, dtype = 'float32')
        self.hr = np.asarray(self.hr, dtype = 'float32')
        self.data_len = len(self.imgs)
        
        
    def __getitem__(self, index):
        # Get image name from the pandas df

        img_path = self.imgs[index]
    
        try:
            img_f= Image.open(img_path)
        except FileNotFoundError:
            print("sample missing use first")
            return self.__getitem__(0)
        
        img = self.transform(img_f)
        bvp = self.bvp[index]
        gt = self.gt[index]
        eda = self.eda[index]
        hr = self.hr[index]
        return {
            'img': img, 
            'bvp': bvp,
            'gt':gt,
            'eda':eda,
            'hr':hr
        }
    
    def __len__(self):
        return int(self.data_len)

class EvalImageLoader(Dataset):
    def __init__(self, transformation):
        self.transform = transformation
        self.imgs=[]
        self.gt = []
        self.bvp = []
        self.eda = []
        self.hr=[]
        pars = os.listdir(eval_path)
        for par in pars:
            game_dir = os.path.join(eval_path, par, "game")
            sensor_dir = os.path.join(eval_path, par, par + "_sensor")

            # game_frame
            file = os.listdir(game_dir)
            file.sort(key=lambda x: int(x[5:].split('.')[0]))

            ground_truth = read_csv(os.path.join(sensor_dir, "ALL.csv"), header=None)
            ground_truth.columns=['frame', 'bvp', 'eda', 'hr', 'stage']

            
            for j in range(len(ground_truth["frame"])):
                stage = classes[ground_truth["stage"][j]]
                
                self.imgs.append(os.path.join(game_dir, f'game_{ground_truth["frame"][j]}.png'))
                self.gt.append(classes[ground_truth["stage"][j]])
                self.bvp.append(ground_truth['bvp'][j])
                self.eda.append(ground_truth['eda'][j])
                self.hr.append(ground_truth['hr'][j])
                j+=1
        self.eda = np.asarray(self.eda, dtype = 'float32')
        self.bvp = np.asarray(self.bvp, dtype = 'float32')
        self.hr = np.asarray(self.hr, dtype = 'float32')
        self.data_len = len(self.imgs)
        
    def __getitem__(self, index):
        # Get image name from the pandas df

        img_path = self.imgs[index]
    
        try:
            img_f= Image.open(img_path)
        except FileNotFoundError:
            print("sample missing use first")
            return self.__getitem__(0)
        
        img = self.transform(img_f)
        bvp = self.bvp[index]
        gt = self.gt[index]
        eda = self.eda[index]
        hr = self.hr[index]
        
        return {
            'img': img, 
            'bvp': bvp,
            'gt':gt,
            'eda':eda,
            'hr':hr
        }
    
    def __len__(self):
        return int(self.data_len)
