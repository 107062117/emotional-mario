import csv

# file is a string of your filename.csv
def delete_duplicate(file):
    with open(file, newline='') as csvfile:
        rows = csv.reader(csvfile)
        
        output = []
        # start is the first index, end is the last
        start = 0
        end = 0
        
        # cur is current event
        cur = ''
        
        init = 1
        for row in rows:
            if init == 1:
                init = 0
                start = int(row[0])
                end = int(row[0])
                cur = row[1]
                continue
            elif int(row[0]) <= end + 25 and row[1] == cur:
                end = int(row[0])
            else:
                middle = str(start)
                if cur != "new_stage":
                    output.append([middle, cur])
                if cur == "flag_reached":
                    output.append([str(start+1), "new_stage"])

                cur = row[1]
                start = int(row[0])
                end = int(row[0])
    
        # output_2 = []
        # for i in range(0, len(output)-1):
        #     if output[i][1] == output[i+1][1] and abs(int(output[i][0]) - int(output[i+1][0])) < 25:
        #         output_2.append(output[i])
        #         i += 1
        with open(file, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            for out in output:
                writer.writerow(out)

#delete_duplicate("participant_0_events.csv")