import os
import os.path as ops
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
from torchvision import models

class resnet50_encoder(nn.Module):
    def __init__(self):
        super(resnet50_encoder, self).__init__()
        resnet50 = models.resnet50(pretrained=True)
        resnet50_layers = list(resnet50.children())[:-1] 
        self.resnet50 = nn.Sequential(*resnet50_layers)
        self.fc1 = nn.Linear(2051, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 6)
        self.drop=nn.Dropout(p=0.2)

    def forward(self, x, bvp, eda, hr):
        output = self.resnet50(x)

        output = torch.flatten(output, 1)
        x1=bvp.unsqueeze(1)
        x2=eda.unsqueeze(1)
        x3=hr.unsqueeze(1)
        output = torch.cat((output, x1), dim=1)
        output = torch.cat((output, x2), dim=1)
        output = torch.cat((output, x3), dim=1)
        output =  F.relu(self.fc1(output))
        output = self.drop(output)
        output =  F.relu(self.fc2(output))
        output =  self.fc3(output)
        return output



